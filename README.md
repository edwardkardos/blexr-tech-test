## Blexr Technical Test - Part II - Edward Kardos

Hello,

Thank you for taking the time today to review part 2 of my technical test for Blexr. The document for Part 1 of the assignment can also be found alongside here.

Given the provided requirements, I decided to do a JUnit (Java) test project in Eclipse with Maven. I have worked with some JUnit frameworks in the past and it was expressed to me that Blexr uses similar. 

NOTE:
- As this project was creating in Eclipse with Maven, it is recommeneded you use Eclipse to open this project and deploy the tests with the JUnit test runner. This should bypass configurations that might need to be made to open the project otherwise.
- I have packed a version of the version 81 Chromedriver executable here (/bin), however if you do not have the same version you may need to replace this binary which should be easily found via Google.

You can find the 3 source files of primary interest, named appropriately after the exercise, in the "src" folder. These files contain the tests and can be easily run from there using Eclipse's "Run as JUnit" test option from any method with the @Test attribute, if you would like to run them from inside the IDE while reviewing them:

- FooterLinksAreValid - Exercise 2.1 validates that all links in the footer of the page are valid. I chose to do this by finding all links which lived under the footer element and then using a HTTP connection to those URLs to validate the response code. This allows us to test this in a generic and resuable way. This test currently accounts for testing all scenarios (links in the footer), which would better be replaced with a proper test data source in a real world scenario.

- RealMoneyLinksHaveCorrectURL - Exercise 2.2 validates that the given links point at the appropriate URL. As the link texts and targets are specified and known, it was simple to implement some resuable logic that given the pair (text, URL), it will validate with Selenium that it can indeed find those links and that they point to the appropriate URL.

- SBOBookieSelectorDesktopDeposits50To200CanClaimBonus - This bonus exercise involves a longer sequence of input events, essentially involving locating and clicking() WebElements in the same order the user might. Most interesting in this implementation is leverage of the XPATH to locate HTML tags by their text content.
