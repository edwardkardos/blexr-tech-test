import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class RealMoneyLinksHaveCorrectURL {

	static String 				chromePath		 = "./bin/chromedriver.exe";
    static String 				homePage		 = "https://www.vegasslotsonline.com/real-money";
    static String 				baseURL			 = "https://www.vegasslotsonline.com";
	static WebDriver 			driver			 = null;
   
	@Test
	void DoesLiveCasinoHaveValidLink() {
		linkTextMatchesURLTest("live casino", "/live-dealer/");
	}
	
	@Test
	void DoesPaymentMethodsHaveValidLink() {
		linkTextMatchesURLTest("payment methods", "/deposit-methods/");
	}
	
	@Test
	void DoesSelectionOfTableGamesHaveValidLink() {
		linkTextMatchesURLTest("selection of table games", "/table-games/");
	}
	
	@Test
	void DoesTopQualityOnlineCasionoSitesHaveValidLink() {
		linkTextMatchesURLTest("top-quality online casino sites", "/reviews/");
	}
	
	@Test
	void DoesNoDepositBusinessHaveValidLink() {
		linkTextMatchesURLTest("no deposit bonuses", "/no-deposit-bonuses/");
	}
	
	void linkTextMatchesURLTest(String linkText, String correctURL) {
		// Arrangements are passed via params
		// Act
		if (!linkTextMatchesURL(linkText, correctURL)) {
			// Assert
			fail(linkText + " does not point to " + correctURL);
		}
	}
	
	boolean linkTextMatchesURL(String linkText, String correctURL) {
		// Find the link using the test
		WebElement link = driver.findElement(By.linkText(linkText));
		
		// Get the URL target attribute of the link
		String linkTarget = link.getAttribute("href");
		
		// Filter out the base URL
		String regex = "\\b"+baseURL+"\\b";
		linkTarget = linkTarget.replaceAll(regex, "");
		
		// Check if the link matches
		System.out.println(linkText+ " points to " +linkTarget);
		
		return linkTarget.equals(correctURL);
	}

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.setProperty("webdriver.chrome.driver", chromePath);
		driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(homePage);
	}
	
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
}
