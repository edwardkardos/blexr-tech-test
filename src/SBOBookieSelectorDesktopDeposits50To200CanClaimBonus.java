import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class SBOBookieSelectorDesktopDeposits50To200CanClaimBonus {

	static String 				chromePath		 = "./bin/chromedriver.exe";
    static String 				homePage		 = "https://www.sbo.net/";
    static String 				claimTargetURL	 = "https://www.sbo.net/";
	static WebDriver 			driver			 = null;
	
	@Test
	void CanDesktopDeposits50To200ClaimBonus(){
		// Find the wizard and open it
		WebElement bookieSelector = driver.findElement(By.className("wizard-toggle"));
		bookieSelector.click();
		
		// Find the Desktop option and select it
		WebElement desktopOption = driver.findElement(By.xpath("//*[text()='Desktop']"));
		desktopOption.click();
		
		// Find the value option and select it
		WebElement valueOption = driver.findElement(By.xpath("//*[text()='�50 - �200']"));
		valueOption.click();
		
		// Find the claim bonus confirmation and select it
		WebElement welcomeBonus = driver.findElement(By.xpath("//*[text()='YES']"));
		welcomeBonus.click();
		
		// Find the claim button
		WebElement claimButton = driver.findElement(By.className("claim-button"));
		
		// Assert the claim button points to the provided URL
		String claimButtonTarget = claimButton.getAttribute("href");
		if(!claimButtonTarget.equals(claimTargetURL)) {
			fail("Claim button does not point at specified URL! " +claimButtonTarget);
		}
	}

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.setProperty("webdriver.chrome.driver", chromePath);
		driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(homePage);
	}
	
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
}
