import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class FooterLinksAreValid {

	static String 				chromePath		 = "./bin/chromedriver.exe";
    static String 				homePage		 = "https://www.vegasslotsonline.com/";
    static String 				menuClassName	 = "bottom-menu";
	static WebDriver 			driver			 = null;
    
	@Test
	void AreAllFooterLinksValid() {
		// Arrange
		
		// Find Footer menu named 
		WebElement footerMenu = driver.findElement(By.className(menuClassName));
		
		// Find links nested under the Footer menu
		List<WebElement> links = footerMenu.findElements(By.tagName("a"));
		
		// Check valid links by looping them and testing
		Iterator<WebElement> iterator = links.iterator();
		boolean fail = false;
		
		// Act
        while(iterator.hasNext()) {
            String url = iterator.next().getAttribute("href");
            System.out.println("Testing "+url+"...");
        
            // Check if the URL is empty
            if(url == null || url.isEmpty()) {
            	fail("URL is either not configured for anchor tag or it is empty");
                continue;
            }
            
            // Check if the URL leaves the domain
            if(!url.startsWith(homePage)) {
                System.out.println("URL belongs to another domain, skipping it.");
                continue;
            }
            
            // Send a request to the URL to validate response
            try {
            	HttpURLConnection urlConnection = (HttpURLConnection)(new URL(url).openConnection());
                urlConnection.setRequestMethod("GET");
                // User agent is required to avoid 403s from direct connection requests
                urlConnection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36");
                urlConnection.connect();
                int respCode = urlConnection.getResponseCode();
                
                // Use the response code to validate the link
                if(respCode >= 400){
                	System.out.println(url+" is a broken link: "+respCode);
                	fail = true;
                }
                else{
                	System.out.println(url+" is a valid link: "+respCode);
                }
                    
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        // Assert
        if(fail) {
        	fail("Footer has broken links!");
        }
	}

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.setProperty("webdriver.chrome.driver", chromePath);
		driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(homePage);
	}
	
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
}
